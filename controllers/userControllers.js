const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");


module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		// Syntax: bcrypt.hashSync(dataToBeEncrypted, salt)
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo
	})

	return User.find({email: reqBody.email}).then(result => {

		if(result.length > 0){
			return ("User Already Registered!");
		}
		// No duplicate email found
		else{
			return newUser.save().then((user, error) =>{
						return ("New User Created");	
				})
		}
	});
	
}



module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>{

		// User does not exist
		if(result == null){
			return ("Incorrect username");
		}
		// User exists
		else{
			// Syntax: bcrypt.compareSync(data, encrypted);
			// reqBody.password & result.password
			// bcrypt.compareSync() return Boolean
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			// If the passwords match the result.
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}
			// Password do not match
			else{
				return ("Incorrect password");
			}
		}

	})
};

module.exports.updateUserStatus = (userId, reqBody) =>{
	// Specify the fields/properties to be updated
	let updatedUserStatus = {
		isAdmin: reqBody.isAdmin
	}

	// Syntax: findByIdAndUpdate(documentId, updatesToBeApplied)
	return User.findByIdAndUpdate(userId, updatedUserStatus).then((statusUpdate, error) =>{
		if(error){
			return false;
		}
		else{
			return true
		}
	})
};
