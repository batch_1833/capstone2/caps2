const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");

router.post("/", auth.verify, (req,res) =>{

	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		productControllers.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	}
	// If the user is not Admin, send a response "You don't have permission on this page!"
	else{
		res.send("You don't have permission on this page!");
	}
	
})

// Route to view all products
router.get("/all", (req, res) =>{
	productControllers.getAllProduct().then(resultFromController => res.send(resultFromController));
})

// Route for retrieving all active products
router.get("/", (req, res)=> {
	productControllers.getAllActive().then(resultFromController => res.send(resultFromController));
})

// Route to view specific product
router.get("/:productId", (req, res) =>{
	productControllers.getProduct(req.params.productId).then(resultFromController => res.send (resultFromController))
})

// Route to update Product
router.put("/:productId", auth.verify, (req, res) =>{
	productControllers.updateProduct(req.params.productId, req.body).then(resultFromController => res.send(resultFromController))
})

// Route to Archive a Product
router.patch("/:productId/archive", auth.verify, (req, res)=>{
	productControllers.archiveProduct(req.params.productId).then(resultFromController => res.send(resultFromController));
})

module.exports = router;