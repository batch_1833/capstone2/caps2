const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderController');
const auth = require('../auth');

// get all orders
router.get('/all', auth.verify, (req, res) => {
	const payload = auth.decode(req.headers.authorization);

	orderController.getAllOrders(payload).then(resultFromController => {
		res.send(resultFromController)
	})
});


// Checkedout Product
router.post('/', auth.verify, (req, res) => {
	const payload = auth.decode(req.headers.authorization);
	
	orderController.checkout(payload).then(resultFromController => {
		res.send(resultFromController)
	})
});

module.exports = router;